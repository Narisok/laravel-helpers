<?php

namespace app\Helpers\Deliveries;
use Illuminate\Support\Facades\Http;
    
use App\Models\DeliveryCity;
use App\Models\DeliveryWarehous;

class Delivery
{
    static public function update()
    {
        $status = Delivery::updateDB(true);
        if($status) {
            $status = Delivery::updateDB(false);
        }
        return [
            'status' => $status,
        ];
    }

    static public function findCities($city_name)
    {
        return DeliveryCity::whereRaw('UPPER(title) LIKE \'%'.mb_strtoupper($city_name, 'UTF-8').'%\'')->get();
    }

    static public function getWarehouses($city_ref)
    {
        return DeliveryWarehous::where('city_ref', $city_ref)->get();
    }

    static public function findWarehouses($city_ref, $warehouse_number)
    {
        return DeliveryWarehous::where('city_ref', $city_ref)
            ->where(function ($q)use($warehouse_number) {
                $q->where('title', 'like', '%'.$warehouse_number.'%')
                ->orWhere('number', 'like', '%'.$warehouse_number.'%');
            })
            ->get();
    }

    static private function updateDB(bool $is_cities)
    {
        $response = Http::get('https://www.delivery-auto.com/api/v4/Public/'.($is_cities ? 'GetAreasList' : 'GetWarehousesList'), [
            'culture' => 'uk-UA',
            'country' => 1,
        ]);
        $resultData = [];
        $status = false;
        if($response->getStatusCode() == 200) {
            if($is_cities) {
                foreach($response->json()['data'] as $city) {
                    DeliveryCity::updateOrCreate(['ref' => $city['id']],[
                        'title' => $city['name'],
                    ]);
                }
            } else {
                foreach($response->json()['data'] as $warehouse) {
                    DeliveryWarehous::updateOrCreate(['ref' => $warehouse['id']],[
                        'title' => $warehouse['name'].' '.$warehouse['address'],
                        'city_ref' => $warehouse['CityId'],
                        'number' => $warehouse['name'],
                    ]);
                }
            }
            
            $status = true;
        }

        return [
            'status' => $status,
        ];
    }
}