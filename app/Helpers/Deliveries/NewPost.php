<?php

namespace app\Helpers\Deliveries;
use Illuminate\Support\Facades\Http;

use App\Models\NewPostCity;
use App\Models\NewPostWarehouse;

use Exception;
    
class NewPost
{
    static public function update()
    {
        $status = NewPost::updateCities();
        if($status) {
            $status = NewPost::updateWarehouses();
        }
        return [
            'status' => $status,
        ];
    }

    static public function findCities($city_name)
    {
        return NewPostCity::whereRaw('UPPER(title) LIKE \'%'.mb_strtoupper($city_name, 'UTF-8').'%\'');
    }

    static public function getWarehouses($city_ref)
    {
        return NewPostWarehouse::where('city_ref', $city_ref);
    }

    static public function findWarehouses($city_ref, $warehouse_number)
    {
        return NewPostWarehouse::where('city_ref', $city_ref)
            ->where(function ($q)use($warehouse_number) {
                $q->where('title', 'like', '%'.$warehouse_number.'%')
                ->orWhere('number', 'like', '%'.$warehouse_number.'%');
            });
    }

    static private function updateWarehouses()
    {
        $data = json_decode('{
            "modelName": "Address",
            "calledMethod": "getWarehouses"
        }');
        $response = Http::post('https://api.novaposhta.ua/v2.0/json/', [$data]);
        $resultData = [];
        $status = false;
        if($response->getStatusCode() == 200) {
            foreach($response->json()['data'] as $warehouse) {
                try{
                NewPostWarehouse::updateOrCreate(['ref' => $warehouse['Ref']],[
                    'title' => substr($warehouse['Description'],0,190),
                    'city_ref' => $warehouse['CityRef'],
                    'number' => $warehouse['Number'],
                ]);
                } catch(Exception $e){dd($e->getMessage());}
            }
            $status = true;
        }

        return [
            'status' => $status,
        ];
    }

    static private function updateCities()
    {
        $data = json_decode('{
            "modelName": "Address",
            "calledMethod": "getCities"
        }');
        $response = Http::post('https://api.novaposhta.ua/v2.0/json/', [$data]);
        $resultData = [];
        $status = false;
        // dd($response->json()[0]['data']);
        if($response->getStatusCode() == 200) {
            foreach($response->json()['data'] as $city) {
                NewPostCity::updateOrCreate(['ref' => $city['Ref']],[
                    'title' => $city['Description'],
                ]);
            }
            $status = true;
        }

        return [
            'status' => $status,
        ];
    }
}
