<?php

namespace app\Helpers\Deliveries;
use Illuminate\Support\Facades\Http;
    

class Sat
{
    static public function getWarehouses($cityRef)
    {
        $response = Http::get('https://api.sat.ua/study/hs/api/v1.0/main/json/getRsp', [
            'language' => 'uk',
            'cityRef' => $cityRef,
        ]);
        // dd($response->json());
        $resultData = [];
        $status = false;
        if($response->getStatusCode() == 200) {
            foreach($response->json()['data'] as $city) {
                array_push($resultData, [
                    'title' => $city['description'],
                    'number' => $city['number'],
                    'ref' => $city['ref'],
                ]);
            }

            $status = true;
        }

        return [
            'status' => $status,
            'data' => $resultData,
        ];
    }

    static public function getCities($cityName)
    {
        $response = Http::get('https://api.sat.ua/study/hs/api/v1.0/main/json/getRsp', [
            'language' => 'uk',
            'searchString' => $cityName,
        ]);
        // dd($response->json());
        $resultData = [];
        $status = false;
        if($response->getStatusCode() == 200) {
            foreach($response->json()['data'] as $city) {
                array_push($resultData, [
                    'title' => $city['description'],
                    'ref' => $city['cityRef'],
                ]);
            }
            
            $status = true;
        }

        return [
            'status' => $status,
            'data' => $resultData,
        ];
    }

}