<?php

	namespace app\Helpers\Libs;

	use Illuminate\Support\Facades\Http;
	use GuzzleHttp\Client;
	use GuzzleHttp\Exception\RequestException;

	/**
	 * Read documentaion
	 *  https://developer.allegro.pl/documentation
	**/
	class Alegro
	{
		
	    private $clientId;

	    private $clientSecret; 

	    private $token; //bearer-token-for-application

	    public  $authToken; //bearer-token-for-user

	    private $redirectUri = 'https://power-auto.gigatest.top/alegro';

	    private $authUrlTest = "https://allegro.pl.allegrosandbox.pl/auth/oauth/token?grant_type=client_credentials";

	    private $authUrl = "https://allegro.pl/auth/oauth/token?grant_type=client_credentials";


	    public function __construct($clientId, $clientSecret)
	    {
	        $this->setClientSecret($clientSecret);
	        
	        $this->setCLientId($clientId);

	        $this->token = $this->getAccessToken();
	        //dd($this->token);
	        return $this;
	    }

	    public function setClientSecret($clientSecret)
	    {
	        $this->clientSecret = $clientSecret;
	        return $this;
	    }

	    public function setCLientId($clientId)
	    {
	        $this->clientId = $clientId;
	        return $this;
	    }
        
        // Get Auth Link for take code
	    public function getAuthLink($clientId)
	    {
	    	$redirectUri = $this->redirectUri;
	        return "https://allegro.pl/auth/oauth/authorize"
	            . "?response_type=code"
	            . "&client_id=$clientId"
	            . "&redirect_uri=$redirectUri";
	    }
        
        // Generate token for User
	    public function generateToken($code, $clientId, $clientSecret, $redirectUri)
	    {
	        $url = 'https://allegro.pl/auth/oauth/token';

			$query = ['grant_type' => 'authorization_code', 'code' => $code, 'redirect_uri' => $redirectUri];
    		$authUrl = $url . '?' . http_build_query($query);

    		$ch = curl_init($authUrl);

		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, [
		    			//'Authorization'   => 'Bearer ' . $this->getToken(),
		                 "Authorization: Basic ".base64_encode("$clientId:$clientSecret"),
		                 "Accept: application/vnd.allegro.public.v1+json"
		    ]);

		    $authResult = curl_exec($ch);
		    $authObject = json_decode($authResult);
		    $this->authToken = $authObject->access_token;

		    return $authObject->access_token;
	  
	    }

	    // Generate token for application
	  	public function getAccessToken()
		{
		   
		    $ch = curl_init($this->authUrl);

		    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		    curl_setopt($ch, CURLOPT_USERNAME, $this->clientId);
		    curl_setopt($ch, CURLOPT_PASSWORD, $this->clientSecret);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		    $tokenResult = curl_exec($ch);
		    $resultCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    curl_close($ch);

		    if ($tokenResult === false || $resultCode !== 200) {
		        exit ("Something went wrong");
		    }

		    $tokenObject = json_decode($tokenResult);
		    return $tokenObject->access_token;
		}
        
        // Get Main category List
        // Need token
		function getMainCategories()
		{
		    $getCategoriesUrl = "https://api.allegro.pl/sale/categories";

		    $ch = curl_init($getCategoriesUrl);

		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, [
		                 "Authorization: Bearer ".$this->token,
		                 "Accept: application/vnd.allegro.public.v1+json"
		    ]);

		    $mainCategoriesResult = curl_exec($ch);
		    $resultCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    curl_close($ch);

		    if ($mainCategoriesResult === false || $resultCode !== 200) {
		        exit ("Something went wrong");
		    }

		    $categoriesList = json_decode($mainCategoriesResult);
		    return $categoriesList;
		}

		// Get subcategories By ID
		function getCategoriesByID($id)
		{
		    $getCategoriesUrl = "https://api.allegro.pl/sale/categories";
		    $query = ['parent.id' => $id];
    		$Url = $getCategoriesUrl . '?' . http_build_query($query);
		    
		    $ch = curl_init($Url);

		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, [
		                 "Authorization: Bearer ".$this->token,
		                 "Accept: application/vnd.allegro.public.v1+json"
		    ]);

		    $mainCategoriesResult = curl_exec($ch);
		    $resultCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    curl_close($ch);

		    if ($mainCategoriesResult === false || $resultCode !== 200) {
		        exit ("Something went wrong");
		    }

		    $categoriesList = json_decode($mainCategoriesResult);

		    return $categoriesList;
		}

	
		// Search offers by CategoryID - test function
		function getOffersSearch($category)
		{
			//'parameter.11323'=>'11323_1', - нові
			//'parameter.11323'=>'11323_2' - уживані
			$url = 'https://api.allegro.pl/offers/listing';
			$query = ['category.id' => $category, 'fallback' => false, 'offset' => '0', 'parameter.11323'=>'11323_1', 'sort' => ''];
    		$getSellersOffersUrl = $url . '?' . http_build_query($query);
	
			$ch = curl_init($getSellersOffersUrl);

		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, [
		                 "Authorization: Bearer ".$this->token,
		                 "Accept: application/vnd.allegro.public.v1+json"
		    ]);


		    $sellerOffers = curl_exec($ch);
		    $resultCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    curl_close($ch);
		    
		    if ($sellerOffers === false || $resultCode !== 200) {
		        exit ("Something went wrong");
		    }

		    $sellerOffersList = json_decode($sellerOffers);
		    dd($sellerOffersList);
			return $sellerOffersList;
		}

		// Search offers by CategoryID
		function OffersSearch($category, $from, $param11323='11323_1', $searchMode = 'REGULAR')
		{
			//'parameter.11323'=>'11323_1', - нові
			//'parameter.11323'=>'11323_2' - уживані
			$url = 'https://api.allegro.pl/offers/listing';
			$query = ['category.id' => $category, 'fallback' => false, 'offset' => $from, 'parameter.11323'=> $param11323, 'sort' => '', 'searchMode' => $searchMode];
    		$getSellersOffersUrl = $url . '?' . http_build_query($query);
	
			$ch = curl_init($getSellersOffersUrl);

		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, [
		                 "Authorization: Bearer ".$this->token,
		                 "Accept: application/vnd.allegro.public.v1+json"
		    ]);


		    $sellerOffers = curl_exec($ch);
		    $resultCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    curl_close($ch);
		    
		    if ($sellerOffers === false || $resultCode !== 200) {
		        exit ("Something went wrong");
		    }

		    $sellerOffersList = json_decode($sellerOffers);
			return $sellerOffersList;
		}



		// Search offers by CategoryID -- test function
		function getOfferData($idOffer)
		{
			$url = 'https://api.allegro.pl/sale/offers/'.$idOffer;
			//$url = 'https://allegro.pl/listing?string='.$idOffer;

			//$query = ['offerId' => $idOffer];
    		//$getSellersOffersUrl = $url . '?' . http_build_query($query);
	
			$ch = curl_init($url);

		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, [
		              	
	                    'Authorization: Bearer ' . $this->token,
	                    'Content-Type: application/vnd.allegro.public.v1+json',
	                    'Accept: application/vnd.allegro.public.v1+json',
	                    'Accept-Language: pl-PL'
		                

		    ]);


		    $sellerOffers = curl_exec($ch);
		    dd($sellerOffers);
		    $resultCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    curl_close($ch);
		    
		    if ($sellerOffers === false || $resultCode !== 200) {
		        exit ("Something went wrong");
		    }

		    $sellerOffersList = json_decode($sellerOffers);
		    dd($sellerOffersList);
			return $sellerOffersList;
		}

		

		function getProductParameters($category)
		{
			$url = 'https://api.allegro.pl/sale/categories/'.$category.'/product-parameters';

			$ch = curl_init($url);
			//dd($this->token);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, [
		                 "Authorization: Bearer ".$this->token,
		                 "Accept: application/vnd.allegro.public.v1+json"
		    ]);



		    $sellerOffers = curl_exec($ch);
		    $resultCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    curl_close($ch);
		    
		    if ($sellerOffers === false || $resultCode !== 200) {
		        exit ("Something went wrong");
		    }

		    $sellerOffersList = json_decode($sellerOffers);
			return $sellerOffersList;
		}


	}
?>