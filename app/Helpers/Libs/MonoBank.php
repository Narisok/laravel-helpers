<?php

    namespace App\Helpers\Libs;
    use Illuminate\Support\Facades\Http;
    use Illuminate\Http\Client\RequestException;

    class MonoBank
    {
        static public function update()
        {
            try {     
            $url = "https://api.monobank.ua/bank/currency";
            $resp = Http::withHeaders([
                'Content-Type'  => 'application/json',])
                ->get($url);
                $response = $resp->getBody()->getContents();
                $arr = json_decode($response, true);

            } catch (RequestException $e) {
                return [
                    'status' => false,
                    'rateBuy' => 7.4,
                    'rateSell' => 7.59,
                ];
            }
            if ($resp->getStatusCode() == 200 AND is_array($arr)){

                foreach($arr as $elem) {
                    if(($elem['currencyCodeA'] ?? 0 ) == 985 AND ($elem['currencyCodeB'] ?? 0) == 980) {
                        $rateBuy = $elem['rateBuy'];
                        $rateSell = $elem['rateSell'];
                    }
                }

            } else {
                return [
                    'status' => false,
                    'rateBuy' => 7.4,
                    'rateSell' => 7.59,
                ];
            }

            return [
                'status' => true,
                'rateBuy' => $rateBuy,
                'rateSell' => $rateSell,
            ];
        }
    }