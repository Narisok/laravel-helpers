<?php

namespace App\Models\Deliveries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryCity extends Model
{
    use HasFactory;

    protected $table = 'delivery_cities';

    protected $fillable = [
        'title',
        'ref',
    ];

    public function warehouses()
    {
        return $this->hasMany(App\Models\DeliveryWarehouse::class, 'city_ref', 'ref');
    }
}
