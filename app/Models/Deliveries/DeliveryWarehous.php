<?php

namespace App\Models\Deliveries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryWarehous extends Model
{
    use HasFactory;

    protected $table = 'delivery_warehouses';

    protected $fillable = [
        'title',
        'city_ref',
        'ref',
        'number',
    ];

    public function city()
    {
        return $this->hasOne(App\Models\DeliveryCity::class, 'ref', 'city_ref');
    }
}
