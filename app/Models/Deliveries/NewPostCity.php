<?php

namespace App\Models\Deliveries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\NewPostWarehouses;

class NewPostCity extends Model
{
    use HasFactory;

    protected $table = 'new_post_cities';

    protected $fillable = [
        'title',
        'ref',
    ];

    public function warehouses()
    {
        return $this->hasMany(NewPostWarehouses::class, 'city_ref', 'ref');
    }
}
