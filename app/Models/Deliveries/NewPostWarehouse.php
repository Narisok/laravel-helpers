<?php

namespace App\Models\Deliveries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\NewPostCity;

class NewPostWarehouse extends Model
{
    use HasFactory;

    protected $table = 'new_post_warehouses';

    protected $fillable = [
        'title',
        'city_ref',
        'ref',
        'number',
    ];

    public function city()
    {
        return $this->hasOne(NewPostCity::class, 'ref', 'city_ref');
    }
}
