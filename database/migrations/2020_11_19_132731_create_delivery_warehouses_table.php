<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_warehouses', function (Blueprint $table) {
            $table->id();
            $table->string('title', 190)->index();
            $table->string('city_ref', 37);
            $table->string('ref', 37)->unique();
            $table->string('number', 190)->index();

            $table->foreign('city_ref')->references('ref')->on('delivery_cities')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_warehouses');
    }
}
