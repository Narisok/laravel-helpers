<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewPostWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_post_warehouses', function (Blueprint $table) {
            $table->id();
            $table->string('title', 190)->index();
            $table->string('city_ref', 37);
            $table->string('ref', 37)->unique();
            $table->string('number', 190)->index();

            $table->foreign('city_ref')->references('ref')->on('new_post_cities')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_post_warehouses');
    }
}
